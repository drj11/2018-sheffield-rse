# Salary

The job is Grade 7 advertised as 31,302 to 39,609 with potential
growth to 43,267.

The Sheffield grades are listed here:
https://www.sheffield.ac.uk/polopoly_fs/1.797564!/file/Aug18salaries.pdf

Grade 7 has 12 points, 9 in the normal range,
and 3 in the "exceptional range".
Point 9 (the highest "normal range") is 39,609.
Each point is, very roughly, 1000 GBP.

Sheffield initially offer 31,302 GBP which is Point 1

# An appropriate scale point to compensate candidate.

The candidate is exceptional and will be a credit to the
department and the institution, providing both growth and
support.

The candidate should be offered a salary at Point 9 on Grade 7.

An offer at this level will re-assure the candidate that the
department is committed to the candidate and the RSE team.

An offer at this level allows candidate to more easily balance this
offer against commercial offerings (while still being below market to
attract a candidate of this calibre in a commercial context).

An offer at this level will reflect the interview panel's
evaluation of the candidate and the panel's desire to appropriately
compensate strong candidates that will grow the RSE team.

Let us review the candidate's proposition:

- Candidate brings more than 20 years' experience
  as a software professional.

- Candidate has a knowledge of software engineering practices
  that is both broad and deep and battle-hardened. Candidate
  will use these to strengthen the RSE team, and the
  institution, in skill areas that are lacking in the
  traditional career paths.

- Candidate brings an infectious joy of learning that will be a
  positive contribution to the RSE teams personal and
  professional growth.

- Candidate's excellent knowledge of software tools (such as git,
  github, ssh, /bin/sh, awk) will polish the RSE team's day-to-day
  practices and enhance the reputation of the RSE team.

- Candidates is an expert in Python and C, far beyond what is
  reasonable for a position at this level.

- Candidate leads a small foundation (Climate Code Foundation)
  and has worked on NASA's surface temperature product, completely
  rewriting it. Such work demonstrates a self-starting
  determination to engage with some of the problematic aspects
  of coding in The Academy.

- Candidate has presented work at local and national conferences
  in talk, workshop, and poster format. Candidate should be
  urged to continue this outreach and be compensated for
  representing the institution in this way.

- Candidate brings an array of skills that will enable the RSE
  group to take on a more diverse range of clients. These
  include but are not limited to: The Programming Language Go,
  which is being used commercially and is enjoying some growth
  in The Academy; 64-bit Intel assembler, which is useful in
  understanding the optimisation of codes for current HPC
  platforms; AVR (such as the ATtiny85) and other embedded
  microcontrollers, which is useful for industrial and medical
  embedded applications, the internet-of-things, and the
  quantified self.

- Candidate has a strong degree in Mathematics from an
  exceptional institution: Cambridge University.

- Candidate has a good generalist background in climate science
  and biology which will give the RSE group additional contact
  surface.

- Candidate has already helped RSE team members in their careers
  and personal growth, showing a commitment to the values of the
  emerging RSE culture.

- Candidate's previous position at this institution was at
  Grade 8, point 5.
