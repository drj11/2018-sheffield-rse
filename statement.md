# Application Statement

I have stood on the sidelines watching while academia struggles
with the increasing burden of overly complex software. The rise
of the Research Software Engineering movement has been a welcome
development. From an industrial perspective I have seen people, teams,
departments, disciplines struggle with software process problems
that engineers in commercial settings have developed solutions to.
Engineering practices that I am familiar with, have advised on,
and have used to help deliver clarity and on-budget solutions to
a variety of software projects.

I would use this position in the Research Software Engineering
team to begin a programme of engaging with, and making
successful use of, modern engineering practices. Cementing the
group's existing knowledge, and planning next steps to ensure
the team continues to grow their engineering competencies.

I believe that Research Software Engineers, as technical
software professionals, are in an excellent position to step in
and assist with the software aspects of modern academic
projects.

I am excited in particular to embed as a software engineer in
The Academy, and relish the prospect of a working in a
collaborative environment with opportunities to work on projects
across a wide range of disciplines.

I believe I can contribute and I would do well.

My CV is available online at https://gitlab.com/drj11/cv


## Declaration of Interest

I know several present and past members of the RSE team, some as
friends, some professionally, and some just as people I say hi
to. I have advised and supported some of them on their current
careers. That assures me that I would find this team a very
positive one to work with.


## Evidenced Criteria

Numbered paragraphs correspond to the numbering scheme used in
the "About The Job" document.

1. Educated to PhD level. Instead of a PhD, I have more than 20
   years experience writing software in a variety of industrial
   and commercial environments.

2. Experience of collaborating with academics and researchers.
   Climate Code Foundation work has involved a collaboration
   with NASA climate scientists to improve the GISTEMP software
   to make it clearer, more portable, and accessible. Some of the
   work at ScraperWiki involved collaborating with academic Horizon
   2020 partners in Italy, where we were providing the
   infrastructure platforms to support the work. In my most
   recent engagement at The University of Sheffield, I
   collaborating with partners at the Harvard T.H. Chan School
   of Public Health and the Harvard Medical School to specify
   and deliver a data infrastructure platform supporting
   researchers in genomics and genomic visualisation.

3. Proven track record of reproducible software development.
   Methodology buzzwords come and go, but a personal commitment
   to software engineering is a lasting thing. I will not write
   software without version control. Today, this means git.
   Gilb's « Principles of Software Engineering Management »
   (1988) describes Evolutionary Delivery which has been a
   pretty good model for all the Agiles and Extremes which have
   been later and more popular. Proper process is essential for
   writing quality software, software that meets client
   requirements, and is distinguished from mere programs by
   being both reliable and well documented.

4. Experience in development and delivery of technical training.
   At Ravenbrook I have directly delivered training in using
   Perforce. At ScraperWiki I delivered in-house training in
   testing methodologies, and the Unix tool awk. I have
   delivered talks, workshops and assisted in tutorials for
   PyCon UK, Floss UK, and a variety of local user groups.
   I have helped at a couple of Software Carpentry workshops,
   including delivering modules on /bin/sh.

5. Significant range of software development experience in
   programming languages relevant to academic research. I am
   independent and confident working in Python and Go. I have
   mastered /bin/sh and awk and relish in the (not so common)
   opportunities to make productive use of this mastery. I am an
   expert in C and the last programmer alive that tries to
   maintain a distinction between C and C++. I have a smattering
   of experience in Java, R. I once worked out how to write a C
   program to call NAG's Fortran library on an IBM 3084Q running
   MVS/TSO.

6. Experience of development for high-performance computing (or
   large scale data analytics). I keep up to date with recent
   developments in (Intel) CPU architecture. I know what the
   "restrict" keyword does in C, and I wish people would use it
   more.

7. A track record of executing and reporting on computational
   research experiments. No experience of research experiments
   directly, but programming is programming, and I have a track
   record of completing industrial and commerical computational
   projects. Some of which have a research component.

8. Ability to assess and organise resources. This is of
   essential in the commercial consultancy work that we did at
   Ravenbrook and ScraperWiki. Planning is the single most
   useful tool to ensure customer satisfaction.

9. Effective communication skills, both written and verbal. In
   all of my jobs back to 2003 I have delivered written and
   verbal reports on project progress, completion, proposals,
   and analyses of competition, vendor solutions, and internal
   projects. I have contributed to written academic reports (for
   internal meetings, grant agencies, and peer-review journal
   articles), commercial reports (for proposal, progress, and
   completion). I have contributed to national and local open
   source based community conferences and user groups.

10. Experience of advocating reproducible research/software
    engineering best practice/the role of RSEs. In my most
    recent engagement at The University of Sheffield, I have
    advocated for reproducible research in my PI's group, and I
    have shared my engineering knowledge with the team. I have
    advocated for engaging with RSEs in the organisation to
    complete various tasks (unsuccessfully).

11. Experience of working within a team. I have worked in teams
    in all of my jobs. Some times I have been personally
    responsible for task planning for particulary projects to
    ensure that team members contribute to the team's success
    and completes projects on time, at other times I have been
    participating with advice. I enjoy that teams give the
    opportunity to learn from one another, and to employ diverse
    skills to achieve things that one programmer alone cannot
    deliver.

12. Ability to motivate and communicate with researchers from
    multidisciplinary backgrounds. It's impossible to
    convincingly engage with academics unless you understand a
    little of their world. I enjoy the collaboration and I'm
    familiar with presenting complex topics in ways that
    outsiders can understand.

13. Ability to develop creative approaches to problem solving
    within research software. I have a track record in industry
    of developing innovative efficient approaches to problem
    solving, some of which are creative. I'm confident at least
    some of that transfers to research software.

14. Ability to analyse and solve technical problems with an appreciation
    of longer-term implications. Engineering is the discipline
    of avoiding harm by ensuring that failures are avoided and
    experience across the field is shared and brought to bear on
    future projects. I live for analysing problems and I take
    particular appreciation in projects and artefacts where we
    can take the time to make well crafted long-lasting
    solutions that have avoided a hazardous pile of monstrous
    hacks.

15. Ability to develop new technical skills. This has been an
    omnipresent feature of my career, and one that I have
    relished. Pretty much every skill I care to list on my CV
    has been self-taught or learnt peer-to-peer. In the case of
    programming languages CoffeeScript and Go we have been
    learning before significant tutorial and learning resources
    were in place. That puts me in a position to appreciate the
    value of peer-to-peer learning, and I'm keen to support
    others in their learning.

16. Experience of developing and maintaining a network of contacts
    throughout own work area. I attend conferences, working
    groups, and local meetups, all of which is helpful for
    maintaining both a formal and informal contacts with
    professionals that share goals, skills, and organisations.
    
